# vipyr - VIrtual PYthon Ready

## vipyr is designed to be a free entry point for anyone interested in learning how to develop in Python. This vipyr image has been pre-built and compiled to allow anyone to work with Python in an environment that should have little, to no impact to your host system.
## To learn how to use this image, and dive into learning Python please check out the Wiki.

# Thanks!

