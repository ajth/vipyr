#vipyr : VIrtual PYthon Ready

## Software Removed:
- Amazon
- Calendar
- Cheese
- File Roller
- Mahjong
- Mines
- Remmina
- Rhythmbox
- Shotwell
- Simple Scan
- Solitaire
- Sudoku
- Thunderbird
- To Do
- Transmission
- Videos

## Software Installed:
- 7zip
- Atom
- Python 3.6.5
- Ubuntu updates

## Ubuntu Settings:
- Auto Suspend - Off
- Blank Screen - Off
- Disable Bluetooth
- Disable Notifications
- Screen Lock - Off
- Configure VS code to add Python on support
